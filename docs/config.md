## Configuration ##

`config/example.conf` is a template you can use to create your own config file. Your config file should be located at `$HOME/.standup-steve/default.yml`.

~~~yml
# Slack API token
token: my_token

bot:
  name: Steve Standup
  emoji: :surfer:
  channel: standup

# Slack usernames for users that will participate in the standup
users:
  - user1
  - user2
  - user3

standup:
  # If set to true, we will not have a standup the first day after a restart
  stoodup: true
  # days to perform standup Sun=0,  Sat=6
  days:
    - 1
    - 2
    - 3
    - 4
    - 5
  # Time for the standup to commence (in 24hour format)
  time: 08:00
  # Standup will be canceled after 'timeout' seconds of inactivity (3600=1 hour)
  # timeout is ignored when debug = true
  timeout: 3600
debug: true
~~~
