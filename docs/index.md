## System Requirements ##

- [Nodejs](https://nodejs.org/en/download/) `v8.0.0+`
- [A Slack Workspace](https://slack.com/get-started)

## Install ##

~~~shell
npm install -g standup-steve
~~~

## Execute ##

Your can run the script directly

~~~shell
standup-steve
~~~

You will probably want to run the script as a daemon. We recommend using [forever](https://www.npmjs.com/package/forever).

~~~shell
npm install -g forever
forever start standup-steve
~~~
