
1.0.5 / 2017-12-15
==================

  * Add update of changelog to npm version
  * Ignore impediments question when none given
  * Add stoodup config option
  * Add try catch to print out uncaught errors
  * Quote every line in a multiline response

v1.0.4 / 2017-11-29
===================

  * 1.0.4
  * Set default values for config
  * Update documentation

v1.0.3 / 2017-11-17
===================

  * 1.0.3
  * Update documentation

v1.0.2 / 2017-11-17
===================

  * 1.0.2
  * Test CI

v1.0.1 / 2017-11-17
===================

  * 1.0.1
  * Add bin script, configure CI
  * Add standup timeout + fix async bugs
  * Update documentation
  * update CI
  * update docs
  * create docs using mkdocs
  * try again
  * try again
  * try again
  * try again
  * fix fork me banner
  * generate installation and user guides
  * make username bold in channel post
  * update the listener code
  * update pages
  * update pages
  * update pages
  * update pages
  * update pages
  * rework README to make it look nicer
  * add outside open link
  * add outside open link
  * fix capitalization
  * more work on project page
  * more work on project page
  * more work on project page
  * add fork ribbon
  * render with stylesheet
  * render with stylesheet
  * render with stylesheet
  * render with stylesheet
  * render with stylesheet
  * add ruby-irb to gitlab-ci.yml
  * add ruby rdoc to gitlab-ci.yml
  * test pages
  * update commets
  * add license file
  * rename image
  * initial commit

n.n.n / 2017-12-15
==================

  * Ignore impediments question when none given
  * Add stoodup config option
  * Add try catch to print out uncaught errors
  * Quote every line in a multiline response
